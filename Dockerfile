FROM golang:1.13 as builder

WORKDIR /go/src/git.eeqj.de/sneak/sircd
COPY . .

#RUN make lint && make build
RUN make build

WORKDIR /go
RUN tar cvfz go-src.tgz src && du -sh *

# this container doesn't do anything except hold the build artifact
# and make sure it compiles.

FROM alpine

COPY --from=builder /go/src/git.eeqj.de/sneak/sircd/sircd /bin/sircd

# put the source in there too for safekeeping
COPY --from=builder /go/go-src.tgz /usr/local/src/go-src.tgz

CMD /bin/sircd

# FIXME add testing
