package main

import "os"
import "fmt"
import "github.com/sirupsen/logrus"
import "git.eeqj.de/sneak/sircd/irc"
import "github.com/spf13/viper"

var Version string
var Builduser string
var Buildarch string

func main() {

	c := viper.New()
	// default config variables
	c.SetDefault("myhostname", "irc.example.com")
	c.SetDefault("network", "ExampleNet")
	c.SetDefault("admin", "webmaster@example.com")
	c.SetDefault("loglevel", "info")
	c.SetDefault("version", Version)
	c.SetDefault("builduser", Builduser)
	c.SetDefault("buildarch", Buildarch)

	// read config file
	c.SetConfigName("sircd")               // name of config file (without extension)
	c.AddConfigPath("/etc/sircd/")         // path to look for the config file in
	c.AddConfigPath("$HOME/.config/sircd") // call multiple times to add many search paths
	c.AddConfigPath(".")                   // optionally look for config in the working directory
	err := c.ReadInConfig()                // Find and read the config file
	if err != nil {                        // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// this is the only config settable by env var by design
	// do everything else in the config file
	if os.Getenv("DEBUG") != "" {
		c.Set("loglevel", "debug")
	}

	// set up logging
	log := logrus.New()
	log.SetReportCaller(false)
	switch c.GetString("loglevel") {
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	case "info":
		log.SetLevel(logrus.InfoLevel)
	default:
		log.SetLevel(logrus.DebugLevel)
		log.SetReportCaller(true)
	}
	// instantiate server
	s := irc.New(c)
	// give it our logger
	s.SetLogger(log)

	// run it
	s.Start()
}
