VERSION := $(shell git rev-parse --short HEAD)
BUILDUSER := $(shell whoami)
BUILDHOST := $(shell hostname -s)
BUILDARCH := $(shell uname -m)

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Builduser=$(BUILDUSER)@$(BUILDHOST)
GOLDFLAGS += -X main.Buildarch=$(BUILDARCH)
GOFLAGS = -ldflags "$(GOLDFLAGS)"

FN := sircd

default: rundebug

rundebug: build
	DEBUG=1 ./$(FN)

run: build
	./$(FN)

build: $(FN)

go-get:
	cd cmd/$(FN) && go get -v

./$(FN): */*.go cmd/*/*.go go-get
	cd cmd/$(FN) && go build -o ../../$(FN) $(GOFLAGS) .

fmt:
	go fmt */*.go
	go fmt cmd/$(FN)/*.go
