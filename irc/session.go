package irc

import log "github.com/sirupsen/logrus"

type ircUserSession struct {
	//FIXME add a mutex and protect during writes
	nick              string
	user              string
	specifiedHostname string
	realName          string
	remoteHost        string
	state             string
	// FIXME make the connection check its state for invalidity periodically
	// and .Kill() it if the irc protocol session wants the client
	// connection gone
	invalid bool
}

func NewIrcUserSession() *ircUserSession {
	// FIXME get conn.RemoteAddr passed in and stringify it here and put it
	// in the session
	s := new(ircUserSession)
	s.nick = "*" //default for s2c messages pre-NICK
	s.state = "init"
	s.invalid = false
	return s
}

func (s *ircUserSession) CheckState(wantedState string) bool {
	if s.state == wantedState {
		return true
	} else {
		return false
	}
}

func (s *ircUserSession) SetState(newState string) bool {
	log.Infof("state changing: %s->%s", s.state, newState) //FIXME remove this
	s.state = newState
	return true
}

func (s *ircUserSession) SetNick(input string) bool {
	// FIXME check for valid nick-ness
	s.nick = input
	return true
}

func (s *ircUserSession) SetUserInfo(params []string) bool {
	if !s.CheckState("init") {
		// can only do this when going init->normal
		return false
	}
	if s.nick == "*" {
		// must set nick first
		return false
	}
	if len(params) >= 4 {
		s.user = params[0]
		s.specifiedHostname = params[1]
		s.realName = params[3]
		s.SetState("normal")
		return true
	} else {
		return false
	}
}
